import NN
import pandas as pd
import numpy as np
model0 = NN.RNN_model()
traindf = pd.read_pickle('./traindata.pkl').sample(frac=1, random_state = 123)
train_lst = traindf.values
# print(traindf.columns[0:20])
# print(traindf.columns[20:40])
# print(traindf.columns[40:80])#20~90 is service
# print(traindf.columns[80:100])
# print(traindf.columns[100:])
trainint_feature = np.array(train_lst[:, :-7])
trainint_feature = trainint_feature.reshape([-1,1,115])
print(trainint_feature.shape)
hisr = model0.fit(input=trainint_feature, label=train_lst[:, -7:-5])
model1 = NN.rRNN_model()
hisrr = model1.fit(input=trainint_feature,label=train_lst[:, -7:-5])
validation1 = hisr['val_loss']
validation2 = hisr['val_loss']
import matplotlib.pyplot as plt
plt.plot(validation1)
plt.plot(validation2)
plt.legend(['Simple RNN','Reduced RNN'])
plt.show()
