from abc import abstractmethod
from keras.models import *
from keras.layers import *
from keras.activations import *
from keras.optimizers import *

SIMPLE_RNN = 0
GRU_TYPE = 1
LSTM_TYPE = 2

class RNN_model:
    def __init__(self, model_path = None,input_shape = [None, 115],rnn_neuron_type = SIMPLE_RNN, rnn_neuron_number = 20, dense_neuron_number = 2,learning_rate = 0.1):
        if not model_path == None:
            self.model = load_model(model_path)
        else:
            input = Input(input_shape)
            rnn_layer = None
            if rnn_neuron_type == SIMPLE_RNN:
                rnn_layer = SimpleRNN(rnn_neuron_number,activation = 'sigmoid')(input)
            elif rnn_neuron_type == GRU_TYPE:
                rnn_layer = GRU(rnn_neuron_number,activation='sigmoid')(input)
            else:
                rnn_layer = LSTM(rnn_neuron_number, activation='sigmoid')(input)
            output = Dense(dense_neuron_number,activation='softmax')(rnn_layer)
            self.optimizer = SGD(lr=learning_rate)
            self.model = Model(input,output)
            self.model.compile(loss='binary_crossentropy',optimizer=self.optimizer)
            self.model.summary()

    def fit(self, input, label, epoch = 100):
        history = self.model.fit(x = input, y=label,batch_size=128, epochs=epoch,validation_split=0.1)
        return history

class rRNN_model(RNN_model):
    def __init__(self, model_path=None, rnn_neuron_type=SIMPLE_RNN, rnn_neuron_number=20,
                 dense_neuron_number=2, learning_rate=0.1):
        if not model_path == None:
            self.model = load_model(model_path)
        else:
            input0 = Input([None, 85])
            hid00 = SimpleRNN(8,activation='sigmoid',return_sequences=True)(input0)
            input1 = Input([None, 11])
            hid01 = SimpleRNN(2,activation='sigmoid',return_sequences=True)(input1)
            input2 = Input([None,19])
            hid02 = SimpleRNN(2,activation='sigmoid',return_sequences=True)(input2)
            hid10 = SimpleRNN(8,activation='sigmoid',return_sequences=True)(hid00)
            hid11 = SimpleRNN(2,activation='sigmoid',return_sequences=True)(hid01)
            hid12 = SimpleRNN(2,activation='sigmoid',return_sequences=True)(hid02)
            con = concatenate([hid10, hid11,hid12])
            output = SimpleRNN(2, activation='softmax')(con)
            self.model = Model([input0,input1,input2],output)
            self.model.compile(optimizer='adam',loss='binary_crossentropy')

    def fit(self, input, label, epoch=100):
        categraied_input_0 = input[:,:,0:7]
        categraied_input_0 = np.concatenate((categraied_input_0,input[:,:,37:40]),axis=2)
        categraied_input_0 = np.concatenate((categraied_input_0, input[:, :, 40:]), axis=2)

        categraied_input_1 = input[:,:,7:18]
        categraied_input_2 = input[:,:,18:37]
        print(categraied_input_2.shape)
        new_input = [categraied_input_0,categraied_input_1,categraied_input_2]
        history = self.model.fit(x=new_input, y=label, batch_size=128, epochs=epoch,validation_split=0.1)
        return history
