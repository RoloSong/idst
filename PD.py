
# coding: utf-8

# In[1]:


import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from time import time
# import seaborn as sns

# get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


train20_nsl_kdd_dataset_path = "NSL_KDD_Dataset/KDDTrain+_20Percent.txt"
train_nsl_kdd_dataset_path = "NSL_KDD_Dataset/KDDTrain.txt"
test_nsl_kdd_dataset_path = "NSL_KDD_Dataset/KDDTest.txt"


# In[3]:


col_names = np.array(["duration","protocol_type","service","flag","src_bytes",
    "dst_bytes","land","wrong_fragment","urgent","hot","num_failed_logins",
    "logged_in","num_compromised","root_shell","su_attempted","num_root",
    "num_file_creations","num_shells","num_access_files","num_outbound_cmds",
    "is_host_login","is_guest_login","count","srv_count","serror_rate",
    "srv_serror_rate","rerror_rate","srv_rerror_rate","same_srv_rate",
    "diff_srv_rate","srv_diff_host_rate","dst_host_count","dst_host_srv_count",
    "dst_host_same_srv_rate","dst_host_diff_srv_rate","dst_host_same_src_port_rate",
    "dst_host_srv_diff_host_rate","dst_host_serror_rate","dst_host_srv_serror_rate",
    "dst_host_rerror_rate","dst_host_srv_rerror_rate","labels","un"])


# In[4]:


def Createdataset(path):
    origin_list = []
    with open(path, encoding='utf-8') as file:
        words = file.readlines()
        origin_list.append(words)
    new_list = []
    for i in origin_list[0]:
        new_list.append(i.split(","))
    
    data_df = pd.DataFrame(new_list, columns=col_names)
    data_df['duration'] = data_df['duration'].astype('double')
    data_df['protocol_type'] = data_df['protocol_type'].astype(str)
    data_df['service'] = data_df['service'].astype(str)
    data_df['flag'] = data_df['flag'].astype(str)
    data_df['src_bytes'] = data_df['src_bytes'].astype('double')
    data_df['dst_bytes'] = data_df['dst_bytes'].astype('double')
    data_df['land'] = data_df['land'].astype('double')
    data_df['wrong_fragment'] = data_df['wrong_fragment'].astype('double')
    data_df['urgent'] = data_df['urgent'].astype('double')
    data_df['hot'] = data_df['hot'].astype('double')
    data_df['num_failed_logins'] = data_df['num_failed_logins'].astype('double')
    data_df['num_compromised'] = data_df['num_compromised'].astype('double')
    data_df['root_shell'] = data_df['root_shell'].astype('double')
    data_df['su_attempted'] = data_df['su_attempted'].astype('double')
    data_df['num_root'] = data_df['num_root'].astype('double')
    data_df['num_file_creations'] = data_df['num_file_creations'].astype('double')
    data_df['num_shells'] = data_df['num_shells'].astype('double')
    data_df['num_access_files'] = data_df['num_access_files'].astype('double')
    data_df['num_outbound_cmds'] = data_df['num_outbound_cmds'].astype('double')
    data_df['is_host_login'] = data_df['is_host_login'].astype('double')
    data_df['is_guest_login'] = data_df['is_guest_login'].astype('double')
    data_df['count'] = data_df['count'].astype('double')
    data_df['srv_count'] = data_df['srv_count'].astype('double')
    data_df['serror_rate'] = data_df['serror_rate'].astype('double')
    data_df['srv_serror_rate'] = data_df['srv_serror_rate'].astype('double')
    data_df['rerror_rate'] = data_df['rerror_rate'].astype('double')
    data_df['srv_rerror_rate'] = data_df['srv_rerror_rate'].astype('double')
    data_df['same_srv_rate'] = data_df['same_srv_rate'].astype('double')
    data_df['diff_srv_rate'] = data_df['diff_srv_rate'].astype('double')
    data_df['srv_diff_host_rate'] = data_df['srv_diff_host_rate'].astype('double')
    data_df['dst_host_count'] = data_df['dst_host_count'].astype('double')
    data_df['dst_host_same_srv_rate'] = data_df['dst_host_same_srv_rate'].astype('double')
    data_df['dst_host_diff_srv_rate'] = data_df['dst_host_diff_srv_rate'].astype('double')
    data_df['dst_host_same_src_port_rate'] = data_df['dst_host_same_src_port_rate'].astype('double')
    data_df['dst_host_srv_diff_host_rate'] = data_df['dst_host_srv_diff_host_rate'].astype('double')
    data_df['dst_host_serror_rate'] = data_df['dst_host_serror_rate'].astype('double')
    data_df['dst_host_srv_serror_rate'] = data_df['dst_host_srv_serror_rate'].astype('double')
    data_df['labels'] = data_df['labels'].astype(str)

    return data_df


# ## Create DataFrame

# In[5]:


train_df = Createdataset(train_nsl_kdd_dataset_path)
train_df.drop('un',axis=1, inplace=True)
print("Shape:",train_df.shape)
train_df.head()

# In[6]:


test_df = Createdataset(test_nsl_kdd_dataset_path)
test_df.drop('un',axis=1, inplace=True)
print("Shape:",test_df.shape)
test_df.head()


# ## Convert Label

# In[7]:


attack_dict = {
    'normal': 'normal',
    
    'back': 'DoS',
    'land': 'DoS',
    'neptune': 'DoS',
    'pod': 'DoS',
    'smurf': 'DoS',
    'teardrop': 'DoS',
    'mailbomb': 'DoS',
    'apache2': 'DoS',
    'processtable': 'DoS',
    'udpstorm': 'DoS',
    
    'ipsweep': 'Probe',
    'nmap': 'Probe',
    'portsweep': 'Probe',
    'satan': 'Probe',
    'mscan': 'Probe',
    'saint': 'Probe',

    'ftp_write': 'R2L',
    'guess_passwd': 'R2L',
    'imap': 'R2L',
    'multihop': 'R2L',
    'phf': 'R2L',
    'spy': 'R2L',
    'warezclient': 'R2L',
    'warezmaster': 'R2L',
    'sendmail': 'R2L',
    'named': 'R2L',
    'snmpgetattack': 'R2L',
    'snmpguess': 'R2L',
    'xlock': 'R2L',
    'xsnoop': 'R2L',
    'worm': 'R2L',
    
    'buffer_overflow': 'U2R',
    'loadmodule': 'U2R',
    'perl': 'U2R',
    'rootkit': 'U2R',
    'httptunnel': 'U2R',
    'ps': 'U2R',    
    'sqlattack': 'U2R',
    'xterm': 'U2R'
}


# In[8]:


def Label2Converter(df_i):
    df_i.loc[df_i['labels'].str.find("normal") == -1, 'labels2'] = 'attack'
    df_i.loc[df_i['labels2'].isnull(), 'labels2'] = 'normal'
    return df_i
    
def Label5Converter(df_i):
    df_i['labels5'] = df_i['labels'].map(attack_dict)
    return df_i


# In[9]:


train_df = Label2Converter(train_df)
train_df = Label5Converter(train_df)


# In[10]:


train_df.head()


# In[11]:


test_df = Label2Converter(test_df)
test_df  = Label5Converter(test_df)


# In[12]:


test_df.head()


# ## Data Analysis

# In[233]:


train_df.groupby('labels2')['labels2'].count()


# In[234]:


train_df.groupby('labels5')['labels5'].count()


# In[235]:


test_df.groupby('labels2')['labels2'].count()


# In[236]:


test_df.groupby('labels5')['labels5'].count()


# In[237]:


pd.crosstab(train_df['protocol_type'], train_df['labels2']).T.style.background_gradient(cmap='summer_r')


# In[238]:


pd.crosstab(train_df['protocol_type'], train_df['labels5']).T.style.background_gradient(cmap='summer_r')


# In[239]:


train_df['service'].unique()


# In[240]:


print(len(train_df['service'].unique()))
pd.crosstab(train_df['service'], train_df['labels2']).T.style.background_gradient(cmap='summer_r')


# In[241]:


pd.crosstab(train_df['service'], train_df['labels5']).T.style.background_gradient(cmap='summer_r')


# In[103]:


print(len(test_df['service'].unique()))
pd.crosstab(test_df['service'], test_df['labels2']).T.style.background_gradient(cmap='summer_r')


# In[104]:


pd.crosstab(test_df['service'], test_df['labels5']).T.style.background_gradient(cmap='summer_r')


# ### drop some service

# In[107]:


ser_col = test_df['service'].unique()

ser_filter = train_df['service'].isin(ser_col)

train_df = train_df[ser_filter]

print(len(train_df['service'].unique()))


# In[242]:


print(len(train_df['flag'].unique()))
pd.crosstab(train_df['flag'], train_df['labels2']).T.style.background_gradient(cmap='summer_r')


# In[243]:


pd.crosstab(train_df['flag'], train_df['labels5']).T.style.background_gradient(cmap='summer_r')


# In[16]:


binary_inx = [6, 11, 13, 14, 20, 21]
binary_cols = col_names[binary_inx].tolist()
train_df[binary_cols].describe()


# In[17]:


pd.crosstab(train_df['su_attempted'], train_df['labels2'])


# ### Remove su_attempted(2.0)

# In[18]:


train_df['su_attempted'] = train_df['su_attempted'].replace(2.0, 0.0)


# In[19]:


test_df['su_attempted'] = test_df['su_attempted'].replace(2.0, 0.0)


# In[20]:


nominal_inx = [1, 2, 3]
numeric_inx = list(set(range(41)).difference(nominal_inx).difference(binary_inx))
numeric_cols = col_names[numeric_inx].tolist()
train_df[numeric_cols].describe()


# In[21]:


pd.crosstab(train_df['num_outbound_cmds'], train_df['labels2'])


# ### Drop num_outbound_cmds

# In[22]:


train_df.drop('num_outbound_cmds',axis=1,inplace=True)
test_df.drop('num_outbound_cmds',axis=1,inplace=True)


# ## One Hot

# In[24]:


def One_Hot(df_i):
    dummies_Protocol = pd.get_dummies(df_i['protocol_type'], prefix='protocol_type')
    dummies_Service = pd.get_dummies(df_i['service'], prefix='service')
    dummies_Flag = pd.get_dummies(df_i['flag'], prefix='flag')
    dummies_labels2 = pd.get_dummies(df_i['labels2'], prefix='labels2')
    dummies_labels5 = pd.get_dummies(df_i['labels5'], prefix='labels5')
    df_o = pd.concat([df_i, dummies_Protocol, dummies_Service, dummies_Flag, dummies_labels2, dummies_labels5], axis=1)
    df_o.drop(['protocol_type', 'service', 'flag', 'labels2', 'labels5'], axis=1, inplace=True)
    
    return df_o


# In[112]:


train_oh = One_Hot(train_df)
train_oh.drop('labels',axis=1,inplace=True)


# In[113]:


test_oh = One_Hot(test_df)
test_oh.drop('labels',axis=1,inplace=True)


# ### reorder

# In[117]:


tempcolumns =  np.array(train_oh.columns)


# In[118]:


#labels2_idx = np.where(tempcolumns == 'labels2')
#print(labels2_idx)


# In[119]:


#labels5_idx = np.where(tempcolumns == 'labels5')
#print(labels5_idx)


# In[122]:


print('tempcol = ')
print(tempcolumns)


# In[123]:


#featue_columns = np.append(tempcolumns[:36], tempcolumns[39:])


# In[125]:


#new_columns = np.append(featue_columns, 'labels2')
#new_columns = np.append(new_columns, 'labels5')


# In[126]:


#new_columns[:-2]


# In[136]:


#new_columns[-2:]


# In[127]:


#train_oh = train_oh[new_columns]


# In[128]:


#list(train_oh.columns.values)


# In[130]:


#test_oh = test_oh[new_columns]


# In[139]:


#len(train_oh.columns)


# In[138]:


#train_oh.columns[3:-2]


# ## Normalization

# In[53]:


from sklearn import preprocessing


# In[153]:


ndary = train_oh.values
train_label = ndary[:,-7:]
Features = ndary[:,:3]
minmax_scale = preprocessing.MinMaxScaler(feature_range=(0,1))
train_scalefeatures = minmax_scale.fit_transform(Features)
train_o = np.concatenate((train_scalefeatures, ndary[:,3:]), axis=1)




# In[155]:


ndary = test_oh.values
test_label = ndary[:,-7:]
Features = ndary[:,:3]
minmax_scale = preprocessing.MinMaxScaler(feature_range=(0,1))
test_scalefeatures = minmax_scale.fit_transform(Features)
test_o = np.concatenate((test_scalefeatures, ndary[:,3:]), axis=1)


# In[159]:


train = pd.DataFrame(train_o)
train.columns = tempcolumns


# In[161]:


test = pd.DataFrame(test_o)
test.columns = tempcolumns

train.to_pickle('./traindata.pkl')
test.to_pickle('./testdata.pkl')
# ## Train Model
